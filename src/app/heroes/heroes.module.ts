import { ListadoComponent } from './listado/listado.component';
import { HeroeComponent } from './heroe/heroe.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    HeroeComponent,
    ListadoComponent
  ],
  exports: [//exporta componentes para visibilidad en el proyecto
    ListadoComponent
  ],
  imports: [ //solo importa modulos
    CommonModule
  ]
})
export class HeroesModule {

}
